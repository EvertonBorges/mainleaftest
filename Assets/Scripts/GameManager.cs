﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{

    private int m_rupees = 0;

    protected override void Init()
    {
        DontDestroyOnLoad(gameObject);

        Manager_Save.ResetPZLBoxes();
    }

    private void OnAddRupees(int value)
    {
        m_rupees += value;

        Observer.GameManager.OnUpdateRupees.Notify(m_rupees);
    }

    private void OnPause()
    {
        Time.timeScale = 0f;
    }

    private void OnUnPause()
    {
        Time.timeScale = 1f;
    }

    private void OnLose()
    {
        Observer.GameManager.OnTransiteToScene.Notify(Scenes.SCN_Segment1);
    }

    private void OnRestart()
    {
        m_rupees = 0;

        Observer.GameManager.OnUpdateRupees.Notify(m_rupees);

        Observer.GameManager.OnTransiteToScene.Notify(Scenes.SCN_Segment1);
    }

    void OnEnable()
    {
        Observer.GameManager.OnPause += OnPause;

        Observer.GameManager.OnUnPause += OnUnPause;

        Observer.GameManager.OnAddRupees += OnAddRupees;

        Observer.GameManager.OnLose += OnLose;

        Observer.GameManager.OnRestart += OnRestart;
    }

    void OnDisable()
    {
        Observer.GameManager.OnPause -= OnPause;

        Observer.GameManager.OnUnPause -= OnUnPause;

        Observer.GameManager.OnAddRupees -= OnAddRupees;

        Observer.GameManager.OnLose -= OnLose;

        Observer.GameManager.OnRestart -= OnRestart;
    }

}
