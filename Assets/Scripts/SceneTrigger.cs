﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTrigger : MonoBehaviour
{

    [SerializeField] private Scenes _toScene = Scenes.SCN_Segment1;

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag(Tags.Player.ToString()))
            return;
        
        Observer.GameManager.OnTransiteToScene.Notify(_toScene);
    }

}
