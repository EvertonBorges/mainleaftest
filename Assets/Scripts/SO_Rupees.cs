﻿using UnityEngine;

[CreateAssetMenu(fileName = "Rupee", menuName = "MainLeaf/Rupee", order = 0)]
public class SO_Rupees : ScriptableObject
{
    [SerializeField] private Material _material = default;
    [SerializeField] private int _value = 1;

    public Material Material => _material;
    public int Value => _value;
}
