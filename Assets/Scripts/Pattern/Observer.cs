﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class Observer
{

    #region Classes

    public interface IEvent
    {
        void Add(Action callback, int order = 0);
    }

    public class Event
    {
        private SortedList<int, List<Action>> _callbackDic = new SortedList<int, List<Action>>();

        public Event Add(Action callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Add(callback);
            else
                _callbackDic.Add(order, new List<Action>() { callback });

            return this;
        }

        public Event Remove(Action callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Remove(callback);

            return this;
        }

        public Event Notify()
        {
            foreach (var keyPair in _callbackDic)
                foreach (var callback in keyPair.Value)
                    callback?.Invoke();

            return this;
        }

        public static Event operator +(Event ev, Action callback) => ev.Add(callback);

        public static Event operator -(Event ev, Action callback) => ev.Remove(callback);
    }

    public class Event<T>
    {
        private SortedList<int, List<Action<T>>> _callbackDic = new SortedList<int, List<Action<T>>>();

        public Event<T> Add(Action<T> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Add(callback);
            else
                _callbackDic.Add(order, new List<Action<T>>() { callback });

            return this;
        }

        public Event<T> Remove(Action<T> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Remove(callback);

            return this;
        }

        public Event<T> Notify(T obj)
        {
            foreach (var keyPair in _callbackDic)
                foreach (var callback in keyPair.Value)
                    callback?.Invoke(obj);

            return this;
        }

        public static Event<T> operator +(Event<T> ev, Action<T> callback) => ev.Add(callback);

        public static Event<T> operator -(Event<T> ev, Action<T> callback) => ev.Remove(callback);
    }

    public class Event<T1, T2>
    {
        private SortedList<int, List<Action<T1, T2>>> _callbackDic = new SortedList<int, List<Action<T1, T2>>>();

        public Event<T1, T2> Add(Action<T1, T2> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Add(callback);
            else
                _callbackDic.Add(order, new List<Action<T1, T2>>() { callback });

            return this;
        }

        public Event<T1, T2> Remove(Action<T1, T2> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Remove(callback);

            return this;
        }

        public Event<T1, T2> Notify(T1 obj1, T2 obj2)
        {
            foreach (var keyPair in _callbackDic)
                foreach (var callback in keyPair.Value)
                    callback?.Invoke(obj1, obj2);

            return this;
        }

        public static Event<T1, T2> operator +(Event<T1, T2> ev, Action<T1, T2> callback) => ev.Add(callback);

        public static Event<T1, T2> operator -(Event<T1, T2> ev, Action<T1, T2> callback) => ev.Remove(callback);
    }

    public class Event<T1, T2, T3>
    {
        private SortedList<int, List<Action<T1, T2, T3>>> _callbackDic = new SortedList<int, List<Action<T1, T2, T3>>>();

        public Event<T1, T2, T3> Add(Action<T1, T2, T3> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Add(callback);
            else
                _callbackDic.Add(order, new List<Action<T1, T2, T3>>() { callback });

            return this;
        }

        public Event<T1, T2, T3> Remove(Action<T1, T2, T3> callback, int order = 0)
        {
            if (_callbackDic.ContainsKey(order))
                _callbackDic[order].Remove(callback);

            return this;
        }

        public Event<T1, T2, T3> Notify(T1 obj1, T2 obj2, T3 obj3)
        {
            foreach (var keyPair in _callbackDic)
                foreach (var callback in keyPair.Value)
                    callback?.Invoke(obj1, obj2, obj3);

            return this;
        }

        public static Event<T1, T2, T3> operator +(Event<T1, T2, T3> ev, Action<T1, T2, T3> callback) => ev.Add(callback);

        public static Event<T1, T2, T3> operator -(Event<T1, T2, T3> ev, Action<T1, T2, T3> callback) => ev.Remove(callback);
    }

    #endregion

    #region Events

    public static class Audio
    {
    }

    public static class Camera
    {
        public static Event<int> OnSwitchCamera = new Event<int>();
    }

    public static class GameManager
    {
        public static Event<int> OnAddRupees = new Event<int>();
        public static Event<int> OnUpdateRupees = new Event<int>();
        public static Event<string> OnShowInteract = new Event<string>();
        public static Event<Scenes> OnTransiteToScene = new Event<Scenes>();
        public static Event OnHideInteract = new Event();
        public static Event OnPause = new Event();
        public static Event OnUnPause = new Event();
        public static Event OnVictory = new Event();
        public static Event OnLose = new Event();
        public static Event OnRestart = new Event();
    }

    public static class Player
    {
        public static Event<Vector2> OnLook = new Event<Vector2>();
        public static Event<Vector2> OnMove = new Event<Vector2>();
        public static Event<float> OnInteract = new Event<float>();
        public static Event<Transform> OnStartInteract = new Event<Transform>();
        public static Event OnEndInteract = new Event();
        public static Event OnJump = new Event();
        public static Event<bool> OnCrouch = new Event<bool>();
        public static Event<Vector3> OnTeleport = new Event<Vector3>();


        public static Event OnPushStart = new Event();
        public static Event OnPushEnd = new Event();

        public static Event OnPullStart = new Event();
        public static Event OnPullEnd = new Event();
    }

    #endregion

}
