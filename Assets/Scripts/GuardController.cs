﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Playables;

public class GuardController : MonoBehaviour
{

    [SerializeField] private PlayableDirector _timeline = null;

    private PlayerController m_player = null;

    void Update()
    {
        if (!m_player)
            return;
        
        var angle = Vector3.SignedAngle(transform.forward, m_player.transform.position - transform.position, Vector3.up);

        angle = Mathf.Abs(angle);

        CheckDistance(angle);
    }

    private void CheckDistance(float angle)
    {
        var distance = angle < 40f ? 4f : (angle < 90f ? 2f : 1f);

        if (Vector3.Distance(transform.position, m_player.transform.position) > distance)
            return;
        
        CheckWall(distance);
    }

    private void CheckWall(float distance)
    {
        var origin = transform.position;

        var direction = m_player.transform.position - transform.position;

        var hits = Physics.RaycastAll(origin, direction, distance);

        var blockableObjects = hits.ToList().FindAll(h => h.transform != transform && h.transform != m_player.transform);

        if (blockableObjects == null || blockableObjects.Count <= 0)
        {
            _timeline.Pause();

            Observer.GameManager.OnLose.Notify();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.TryGetComponent<PlayerController>(out PlayerController player))
            return;
        
        m_player = player;
    }

    void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag(Tags.Player.ToString()))
            return;
        
        m_player = null;
    }

}
