﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : Singleton<PlayerController>
{

    [SerializeField] private PlayerAnimationController m_animator = null;

    [Header("Parameters")]
    [SerializeField] private float _jumpForce = 8f;
    [SerializeField] private float _speed = 2f;
    [SerializeField] private float _crounchedFactorSpeed = 0.5f;
    [SerializeField] private float _turnSmoothTime = 0.1f;

    private Transform m_camera = null;
    private Rigidbody m_rigidbody = null;
    private CapsuleCollider m_capsuleCollider = null;

    private int m_grabDirection = 0;

    private float m_turnSmoothVelocity = 0f;
    private float m_realSpeed => _speed * (m_isCrouched ? _crounchedFactorSpeed : 1f);

    private Vector3 m_move = default;
    private Vector3 m_moveInput = default;

    private bool m_isOnGround = false;
    private bool m_isCrouched = false;
    private bool m_isGrabing = false;
    private bool m_canMoveDuringGrab = false;
    private bool m_canMove = true;

    private Coroutine m_grabCoroutine = null;

    protected override void Init()
    {
        m_rigidbody = GetComponent<Rigidbody>();

        m_capsuleCollider = GetComponent<CapsuleCollider>();

        if (m_animator == null) m_animator = GetComponent<PlayerAnimationController>();
    }

    protected override void StartInit()
    {
        m_camera = Camera.main.transform;
    }

    void Update()
    {
        OnFixRotation();

        OnCheckGround();

        OnAnimations();
    }

    void FixedUpdate()
    {
        OnMove();
    }

    private void OnCheckGround()
    {
        m_isOnGround = Physics.Raycast(transform.position + Vector3.up * 0.05f, Vector3.down, 0.1f);
    }

    private void OnAnimations()
    {
        m_animator.SetIsGrounded(m_isOnGround);

        m_animator.SetIsCrouched(m_isCrouched);

        m_animator.SetIsGrabing(m_isGrabing);

        m_animator.SetYSpeed(m_rigidbody.velocity.y);
    }

    private void OnFixRotation()
    {
        if (!m_canMove)
            return;

        if (m_isGrabing)
        {
            m_move = m_moveInput;

            return;
        }

        if (m_canMoveDuringGrab)
            return;

        if (m_moveInput.magnitude >= 0.25f)
        {
            var targetAngle = Mathf.Atan2(m_moveInput.x, m_moveInput.z) * Mathf.Rad2Deg + m_camera.eulerAngles.y;

            var angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref m_turnSmoothVelocity, _turnSmoothTime);

            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            m_move = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
        }
        else
            m_move = Vector3.zero;
    }

    private void OnMove()
    {
        if (!m_canMove)
            return;
        
        if (m_isGrabing)
        {
            if (!m_canMoveDuringGrab)
                return;

            GrabMove();

            return;
        }

        if (m_canMoveDuringGrab)
            return;

        var velocity = m_move * m_realSpeed;

        velocity = new Vector3(velocity.x, 0f, velocity.z);

        m_rigidbody.angularVelocity = Vector3.zero;

        m_rigidbody.velocity = velocity + Vector3.up * m_rigidbody.velocity.y;

        m_animator.SetSpeed(velocity.magnitude);
    }

    private void GrabMove()
    {
        m_grabDirection = m_move.z > 0f ? 1 : (m_move.z == 0f ? 0 : -1);

        if (m_grabDirection == 1)
            Observer.Player.OnPushStart.Notify();
        else if (m_grabDirection == -1)
            Observer.Player.OnPullStart.Notify();
        else
        {
            Observer.Player.OnPushEnd.Notify();

            Observer.Player.OnPullEnd.Notify();
        }

        m_animator.SetGrabDirection(m_grabDirection);
    }

    private void OnMove(Vector2 value)
    {
        m_moveInput = new Vector3(value.x, 0f, value.y);
    }

    private void OnJump()
    {
        if (!m_isOnGround)
            return;

        m_rigidbody.AddForce(Vector3.up * _jumpForce, ForceMode.Impulse);

        m_isOnGround = false;
    }

    private void OnCrouch(bool value)
    {
        m_isCrouched = value;

        m_canMove = false;

        m_capsuleCollider.center *= m_isCrouched ? 0.5f : 2f;

        m_capsuleCollider.height *= m_isCrouched ? 0.5f : 2f;
    }

    private void GrabStart()
    {
        m_canMoveDuringGrab = true;
    }

    private void OnStartInteract(Transform parent) 
    {
        m_rigidbody.isKinematic = true;

        m_isGrabing = true;

        transform.SetParent(parent);

        m_animator.TriggerGrabing();

        m_animator.SetGrabDirection(0);
    }

    private void OnEndInteract() 
    {
        m_rigidbody.isKinematic = false;

        m_isGrabing = false;

        transform.SetParent(null);

        m_canMoveDuringGrab = true;

        if (m_grabCoroutine != null)
            StopCoroutine(m_grabCoroutine);

        m_grabCoroutine = StartCoroutine(WaitEndGrab());
    }

    private IEnumerator WaitEndGrab()
    {
        float duration = m_grabDirection < 0 ? 1.25f : (m_grabDirection > 0 ? 1f : 0.25f);

        yield return new WaitForSeconds(duration);

        m_canMoveDuringGrab = false;

        Observer.Player.OnPushEnd.Notify();

        Observer.Player.OnPullEnd.Notify();

        m_grabCoroutine = null;
    }

    private void OnTeleport(Vector3 value)
    {
        transform.position = value;
    }

    void OnEnable()
    {
        Observer.Player.OnMove += OnMove;
        
        Observer.Player.OnJump += OnJump;

        Observer.Player.OnCrouch += OnCrouch;

        Observer.Player.OnStartInteract += OnStartInteract;
        
        Observer.Player.OnEndInteract += OnEndInteract;

        Observer.Player.OnTeleport += OnTeleport;
    }

    void OnDisable()
    {
        Observer.Player.OnMove -= OnMove;
        
        Observer.Player.OnJump -= OnJump;
        
        Observer.Player.OnCrouch -= OnCrouch;
        
        Observer.Player.OnStartInteract -= OnStartInteract;
        
        Observer.Player.OnEndInteract -= OnEndInteract;

        Observer.Player.OnTeleport -= OnTeleport;
    }

}
