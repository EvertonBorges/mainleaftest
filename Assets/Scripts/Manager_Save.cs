﻿using UnityEngine;

public static class Manager_Save
{
    
    public static void PZLBoxesSolved()
    {
        PlayerPrefs.SetInt("PZL_Boxes", 1);
    }

    public static bool IsPZLBoxesSolved()
    {
        if (PlayerPrefs.HasKey("PZL_Boxes") && PlayerPrefs.GetInt("PZL_Boxes") == 1)
            return true;

        return false;
    }

    public static void ResetPZLBoxes()
    {
        PlayerPrefs.DeleteKey("PZL_Boxes");
    }

}
