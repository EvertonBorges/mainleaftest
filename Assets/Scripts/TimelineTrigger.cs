﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

public class TimelineTrigger : MonoBehaviour
{

    [SerializeField] private PlayableDirector _timeline = null;

    [SerializeField] private UnityEvent _event = null;

    private PlayerController m_player;

    private void OnInteract(float value)
    {
        if (!m_player)
            return;
        
        _timeline.Play();

        _event.Invoke();
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.TryGetComponent<PlayerController>(out PlayerController player))
            return;

        Observer.GameManager.OnShowInteract.Notify("Enter");

        m_player = player;
    }

    void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag(Tags.Player.ToString()))
            return;

        Observer.GameManager.OnHideInteract.Notify();

        m_player = null;
    }

    public void OnPuzzleSolved()
    {
        Manager_Save.PZLBoxesSolved();
    }

    void OnEnable()
    {
        Observer.Player.OnInteract += OnInteract;
    }

    void OnDisable()
    {
        Observer.Player.OnInteract -= OnInteract;
    }

}
