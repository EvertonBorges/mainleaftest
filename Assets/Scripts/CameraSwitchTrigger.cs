﻿using UnityEngine;

public class CameraSwitchTrigger : MonoBehaviour
{

    [SerializeField] private int _cameraIndex = 0;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag(Tags.Player.ToString()))
            return;
        
        Observer.Camera.OnSwitchCamera.Notify(_cameraIndex);
    }

}
