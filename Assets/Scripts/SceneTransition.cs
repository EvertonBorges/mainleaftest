using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : Singleton<SceneTransition>
{

    [SerializeField] private FadeEffect _canvas = null;

    private Coroutine m_transiteCoroutine = null;

    protected override void Init()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void OnTransiteToScene(Scenes value)
    {
        if (m_transiteCoroutine != null)
            return;

        m_transiteCoroutine = MonoBehaviorHelper.StartCoroutine(GoToScene(value));
    }

    private IEnumerator GoToScene(Scenes scene, float transitionTime = 0.5f)
    {
        var asyncScene = SceneManager.LoadSceneAsync(scene.ToString());

        bool showCanvas = false;

        asyncScene.allowSceneActivation = false;

        _canvas.HideForced();

        _canvas.FadeIn(() => showCanvas = true);

        while (!showCanvas)
            yield return null;

        if (scene == Scenes.SCN_Victory)
            Observer.GameManager.OnVictory.Notify();

        while (asyncScene.progress < 0.9f)
            yield return null;

        asyncScene.allowSceneActivation = true;

        _canvas.FadeOut(() => m_transiteCoroutine = null);
    }

    void OnEnable()
    {
        Observer.GameManager.OnTransiteToScene += OnTransiteToScene;
    }

    void OnDisable()
    {
        Observer.GameManager.OnTransiteToScene -= OnTransiteToScene;
    }

}
