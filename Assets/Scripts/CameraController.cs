﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraController : Singleton<CameraController>
{

    [SerializeField] private CinemachineMixingCamera _mixingCamera = null;
    [SerializeField] private AnimationCurve _blend = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

    private int m_cameraIndex = 0;

    private void OnSwitchCamera(int index)
    {
        if (index == m_cameraIndex)
            return;

        StartCoroutine(SwitchCamera(index));
    }

    private IEnumerator SwitchCamera(int index)
    {
        float currentTime = 0f;

        float duration = _blend.keys[_blend.keys.Length - 1].time;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;

            var value = _blend.Evaluate(currentTime);

            if (m_cameraIndex >= 0) _mixingCamera.SetWeight(m_cameraIndex, 1f - value);

            _mixingCamera.SetWeight(index, value);

            yield return null;
        }

        if (m_cameraIndex >= 0) _mixingCamera.SetWeight(m_cameraIndex, 0f);

        _mixingCamera.SetWeight(index, 1f);

        yield return null;

        m_cameraIndex = index;
    }

    void OnEnable()
    {
        Observer.Camera.OnSwitchCamera += OnSwitchCamera;
    }

    void OnDisable()
    {
        Observer.Camera.OnSwitchCamera -= OnSwitchCamera;
    }

}
