﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class PuzzleBoxesChecker : MonoBehaviour
{

    [SerializeField] private GameObject[] _objectsToDisable = null;
    [SerializeField] private GameObject[] _objectsToEnable = null;

    [SerializeField] private Transform _positionToRestart = null;

    void Start()
    {
        CheckSolution();
    }

    private void CheckSolution()
    {
        if (!Manager_Save.IsPZLBoxesSolved())
            return;

        _objectsToDisable?.ToList().ForEach(o => o.SetActive(false));

        _objectsToEnable?.ToList().ForEach(o => o.SetActive(true));

        Observer.Player.OnTeleport.Notify(_positionToRestart.position);
    }

}
