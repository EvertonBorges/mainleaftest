﻿using UnityEngine;

public class RupeesController : MonoBehaviour
{

    [SerializeField] private SO_Rupees _rupeeReference = default;

    void OnValidate()
    {
        if (_rupeeReference == null)
            return;

        GetComponent<MeshRenderer>().material = _rupeeReference.Material;
    }
    
    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag(Tags.Player.ToString()) || _rupeeReference == null)
            return;
        
        Observer.GameManager.OnAddRupees.Notify(_rupeeReference.Value);

        Destroy(gameObject);
    }

}
