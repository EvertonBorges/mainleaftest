﻿using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour
{

    [SerializeField] private Rigidbody _rigidbody = null;
    [SerializeField] private List<BoxInteractTrigger> _triggers = null;

    private Vector3 m_grabDirection = default;

    private bool m_isInteractable = false;
    private bool m_isGrabing = false;
    private bool m_isPushing = false;
    private bool m_isPulling = false;

    void Awake()
    {
        if (!_rigidbody) _rigidbody = GetComponent<Rigidbody>();

        if (_triggers == null || _triggers.Count <= 0) 
        {
            _triggers = new List<BoxInteractTrigger>();

            _triggers.AddRange(GetComponentsInChildren<BoxInteractTrigger>());
        }

        _rigidbody.isKinematic = true;
    }

    void Update()
    {
        if (!m_isInteractable)
        {
            if (m_isGrabing)
            {
                Observer.Player.OnEndInteract.Notify();

                m_isGrabing = false;

                _rigidbody.velocity = Vector3.zero;

                EnableTrigger(null);
            }

            return;
        }
    }

    void FixedUpdate()
    {
        if (m_isGrabing && m_grabDirection != default && 
            (m_isPushing || m_isPulling) && 
            _rigidbody.velocity != m_grabDirection)
            _rigidbody.velocity = m_grabDirection * (m_isPushing ? 1f : -1f);
    }

    public void IsGrabing(bool value)
    {
        m_isGrabing = value;

        _rigidbody.isKinematic = !value;
    }

    public void SetPushDirection(Vector3 value)
    {
        m_grabDirection = value;
    }

    public void EnableTrigger(BoxInteractTrigger value)
    {
        if (value)
        {
            _triggers.ForEach(t => t.SetColliderActive(false));
            value.SetColliderActive(true);
        }
        else
            _triggers.ForEach(t => t.SetColliderActive(true));
    }

    private void OnPushStart()
    {
        m_isPushing = true;
    }

    private void OnPushEnd()
    {
        m_isPushing = false;
    }

    private void OnPullStart()
    {
        m_isPulling = true;
    }

    private void OnPullEnd()
    {
        m_isPulling = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag(Tags.InteractableArea.ToString()))
            return;

        m_isInteractable = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag(Tags.InteractableArea.ToString()))
            return;

        m_isInteractable = false;

        _rigidbody.isKinematic = false;
    }

    void OnEnable()
    {
        Observer.Player.OnPushStart += OnPushStart;
        Observer.Player.OnPushEnd += OnPushEnd;
        
        Observer.Player.OnPullStart += OnPullStart;
        Observer.Player.OnPullEnd += OnPullEnd;
    }

    void OnDisable()
    {
        Observer.Player.OnPushStart -= OnPushStart;
        Observer.Player.OnPushEnd -= OnPushEnd;

        Observer.Player.OnPullStart -= OnPullStart;
        Observer.Player.OnPullEnd -= OnPullEnd;
    }

}
