﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerAnimationController : MonoBehaviour
{
    
    private const string IS_GROUNDED = "isGrounded";
    private const string IS_CROUNCHED = "isCrouched";
    private const string IS_GRABING = "isGrabing";
    private const string GRABING = "Grabing";
    private const string Y_SPEED = "ySpeed";
    private const string SPEED = "speed";
    private const string GRAB_DIRECTION = "GrabDirection";

    [SerializeField] private Animator m_animator = null;

    private void Awake() {
        if (m_animator == null) m_animator = GetComponent<Animator>();
    }

    public void SetIsGrounded(bool value)
    {
        m_animator.SetBool(IS_GROUNDED, value);
    }

    public void SetIsCrouched(bool value)
    {
        m_animator.SetBool(IS_CROUNCHED, value);
    }

    public void SetIsGrabing(bool value)
    {
        m_animator.SetBool(IS_GRABING, value);
    }

    public void SetYSpeed(float value)
    {
        m_animator.SetFloat(Y_SPEED, value);
    }

    public void SetSpeed(float value)
    {
        m_animator.SetFloat(SPEED, value);
    }

    public void SetGrabDirection(int value)
    {
        m_animator.SetInteger(GRAB_DIRECTION, value);
    }

    public void TriggerGrabing()
    {
        m_animator.SetTrigger(GRABING);
    }

}
