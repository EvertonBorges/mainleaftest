﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxInteractTrigger : MonoBehaviour
{

    [SerializeField] private BoxController _parent;
    [SerializeField] private BoxCollider _collider;

    private PlayerController m_player = null;

    private bool m_isGrabing = false;

    void Awake()
    {
        if (!_parent) _parent = GetComponentInParent<BoxController>();

        if (!_collider) _collider = GetComponentInParent<BoxCollider>();
    }

    public void SetColliderActive(bool value)
    {
        _collider.enabled = value;
    }

    private void OnInteract(float value)
    {
        bool grabing = value > 0f;

        if (m_player == null)
            return;

        if (!m_isGrabing && !grabing)
        {
            _parent.IsGrabing(false);

            _parent.EnableTrigger(null);

            Observer.Player.OnEndInteract.Notify();

            return;
        }

        m_isGrabing = grabing;

        var direction = (_parent.transform.position - transform.position).normalized;

        m_player.transform.rotation = Quaternion.LookRotation(direction, Vector3.up);

        if (m_isGrabing) Observer.Player.OnStartInteract.Notify(_parent.transform);
        else Observer.Player.OnEndInteract.Notify();

        var distance = _parent.transform.position - direction * 1.1f;

        distance.y = m_player.transform.position.y;

        m_player.transform.position = distance;

        _parent.IsGrabing(m_isGrabing);

        _parent.SetPushDirection(direction);

        _parent.EnableTrigger(m_isGrabing ? this : null);
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.TryGetComponent<PlayerController>(out PlayerController player))
            return;

        Observer.GameManager.OnShowInteract.Notify("Grab");

        m_player = player;
    }

    void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag(Tags.Player.ToString()))
            return;

        Observer.GameManager.OnHideInteract.Notify();

        m_player = null;
    }

    void OnEnable()
    {
        Observer.Player.OnInteract += OnInteract;
    }

    void OnDisable()
    {
        Observer.Player.OnInteract -= OnInteract;
    }

}
