﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Manager_Canvas : Singleton<Manager_Canvas>
{

    [SerializeField] private Text _TXT_Rupees = null;
    [SerializeField] private Text _TXT_Interaction = null;

    [SerializeField] private GameObject _CTN_Pause = null;
    [SerializeField] private Button _BTN_Resume = null;

    [SerializeField] private GameObject _CTN_Victory = null;

    private Coroutine m_coroutineInteract;

    protected override void Init()
    {
        DontDestroyOnLoad(gameObject);

        _TXT_Rupees.text = "0";

        _TXT_Interaction.text = "";
    }

    public void BTN_Resume()
    {
        UnPause();
    }

    public void BTN_Restart()
    {
        UnPause();

        Manager_Save.ResetPZLBoxes();

        Observer.GameManager.OnRestart.Notify();
    }

    public void BTN_Quit()
    {
        UnPause();

        Manager_Save.ResetPZLBoxes();

#if UNITY_EDITOR
        UnityEditor.EditorApplication.ExitPlaymode();
        return;
#endif
        Application.Quit();
    }

    private void UnPause()
    {
        _CTN_Pause.SetActive(false);

        _CTN_Victory.SetActive(false);

        Observer.GameManager.OnUnPause.Notify();
    }

    private void UpdateRupeesText(int value)
    {
        _TXT_Rupees.text = value.ToString();
    }

    private void OnShowInteract(string value)
    {
        OnChangeInteractText(value);
    }

    private void OnHideInteract()
    {
        OnChangeInteractText("");
    }

    private void OnVictory()
    {
        _CTN_Victory.SetActive(true);
    }

    private void OnChangeInteractText(string value, float duration = 0.25f)
    {
        if (m_coroutineInteract != null)
            StopCoroutine(m_coroutineInteract);

        m_coroutineInteract = StartCoroutine(ChangeInteractionText(value, duration));
    }

    private IEnumerator ChangeInteractionText(string value, float duration)
    {
        _TXT_Interaction.transform.eulerAngles = Vector3.zero;

        var currentTime = 0f;

        while(currentTime < duration)
        {
            currentTime += Time.deltaTime;

            var xRotation = Mathf.Lerp(0f, 90f, currentTime / duration);

            _TXT_Interaction.transform.eulerAngles = Vector3.right * xRotation;

            yield return null;
        }

        _TXT_Interaction.transform.eulerAngles = Vector3.right * 90f;

        yield return null;

        _TXT_Interaction.text = value;

        yield return new WaitForSeconds(0.05f);

        currentTime = 0f;

        while(currentTime < duration)
        {
            currentTime += Time.deltaTime;

            var xRotation = Mathf.Lerp(90f, 0f, currentTime / duration);

            _TXT_Interaction.transform.eulerAngles = Vector3.right * xRotation;

            yield return null;
        }

        _TXT_Interaction.transform.eulerAngles = Vector3.zero;

        yield return null;
    }

    private void OnPause()
    {
        _CTN_Pause.SetActive(true);

        _BTN_Resume.Select();
    }

    void OnEnable()
    {
        Observer.GameManager.OnUpdateRupees += UpdateRupeesText;

        Observer.GameManager.OnShowInteract += OnShowInteract;

        Observer.GameManager.OnHideInteract += OnHideInteract;

        Observer.GameManager.OnPause += OnPause;

        Observer.GameManager.OnVictory += OnVictory;
    }

    void OnDisable()
    {
        Observer.GameManager.OnUpdateRupees -= UpdateRupeesText;

        Observer.GameManager.OnShowInteract -= OnShowInteract;

        Observer.GameManager.OnHideInteract -= OnHideInteract;

        Observer.GameManager.OnPause -= OnPause;

        Observer.GameManager.OnVictory -= OnVictory;
    }

}
